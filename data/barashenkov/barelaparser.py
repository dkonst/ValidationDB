#/usr/bin/env python

import re
import sys

f=  open(sys.argv[1], 'r')
lines = map(lambda x : x.strip(), f.readlines())

f.close()

tables = []

tmp = []
for l in lines:
  if 'hline' not in l:
    tmp.append(l)
  else:
    tables.append(tmp)
    tmp = []

tables = filter(lambda x: 'Nucleus' not in x[0], tables)
tables = filter(lambda x: 'cite' in ' '.join(x), tables)

def parseXY(xraw, coeff = 1):
  if '\pm' in xraw:
    xval = xraw.split('\pm')[0].strip()
    xerrp = xraw.split('\pm')[1].strip()
    xerrm = xerrp
  elif len(re.findall('(.*)\^{(.+)}_{(.+)}',xraw)) != 0:
    r = re.findall('(.*)\^{(.+)}_{(.+)}',xraw)
    xval = r[0][0]
    xerrp = r[0][1]
    xerrm = r[0][2]
  elif len(re.findall('(.*)\^{(.+)}.*',xraw)) != 0:
    r = re.findall('(.*)\^{.*}.*',xraw)
    xval = r[0]
    xerrp = 0
    xerrm = 0
  else:
    xval = 0 if xraw.strip() == "" else xraw.strip()
    xval = xval
    xerrp = 0
    xerrm = 0
  def post(s):
    s = str(s)
    s = re.sub('[<>]|\\\\[gl]e[q]|\\\\cong|\\\\sim', '', s)
    if len(re.findall('(.*)\^{.+\)}.*',s)) != 0:
      r = re.findall('(.*)\^{.*\)}.*',s)
      s = r[0]
    return s
  xval = post(xval)
  xerrp = post(xerrp)
  xerrm = post(xerrm)
  xval = float(xval) * coeff
  xerrp = float(xerrp) * coeff
  xerrm = float(xerrm) * coeff
  return (xval, xerrp, xerrm)

tabledata = []
for table in tables:
#  print "="*80
  coeff = 1
  points = []
  for line in table:
    cells = line.split('&') 
    if cells[0] != "":
      r = re.findall('.*\^{?(.+)}?\$(.+).*', cells[0].strip())
      if len(r) != 0:
        target = "{0}-{1}".format(r[0][1].strip(), r[0][0].strip())
    if coeff == 1 and len(filter(lambda x: 'GeV' in x, cells)) != 0:
#      print "# set coeff to 1000, GeV found"
      coeff = 1000
    try:
      # parse x
      xraw = cells[1].split('$')[1].replace('(','').replace(')','')
      x = parseXY(xraw, coeff)
      # parse y
      yraw = cells[2].split('$')[1].replace('(','').replace(')','')
      y = parseXY(yraw, coeff) 
      if 'cite' in cells[-1]:
        cite_id = re.findall('.*{(.+)}', cells[-1])[0]
        cite_id = cite_id.split(',')
  #    print target, cite_id
      points.append((x,y, cite_id))
    except Exception as e:
      print "# cannot parse line:\n  ", line, '\n  ', e
  tabledata.append((target, points))

# convert table per cite ...
#cilist = map (lambda x: map(lambda b: b[2], x[1]),tabledata)
#def getelements(l):
#  if l.__class__ == list:
#    for i in l:
#      for k in getelements(i):
#        yield k
#  else:
#    yield l
#cilist = list(set(getelements(cilist)))
#for ci in cilist:

#sys.exit(0)

import sqlite3

db = sqlite3.connect (sys.argv[2])
cur = db.cursor()
type = 'total'
particle = 'n'

for t in tabledata:
  records = []
  for q in t[1]:
    records += q[2]
  records = list(set(records))
  for r in records:
    type_id = cur.execute("select type_id from types where type = 'plot'").fetchone()[0]
    cur.execute("insert into records (type) values ({0})".format(type_id))
    rec_id = cur.execute('select max(rec_id) from records').fetchone()[0]
    target = t[0]
    cur.execute("insert into plots values ({recid}, '{xlabel}', '{ylabel}', '{projectile}','{target}', '{comment}')".format(
      recid = rec_id,
      xlabel = "MeV",
      ylabel = "\sigma " + type,
      projectile = particle,
      target = target,
      comment = ""
    ))
    # point relative to cite
    for p in filter(lambda x: r in x[2], t[1]):
      cur.execute('insert into data (rec_id, xval, yval, xerrm, xerrp, yerrm, yerrp) values ({recid}, {xval}, {yval}, {xerrm}, {xerrp}, {yerrm}, {yerrp})'.format(
      recid = rec_id,
      xval = p[0][0],
      yval = p[1][0],
      xerrm = p[0][2],
      xerrp = p[0][1],
      yerrm = p[1][2],
      yerrp = p[1][1]
    ))
      point_id = cur.execute('select max(point_id) from data').fetchone()[0]
      cur.execute('insert into cite_points values ({0}, {1})'.format(int(point_id), int(r)))
    
db.commit()
db.close()
