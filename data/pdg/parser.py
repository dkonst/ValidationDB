#!/usr/bin/env python
import sys
import re
import time
import sqlite3

f = open(sys.argv[2], 'r')
db = sqlite3.connect(sys.argv[1])
cur = db.cursor()

xstype = sys.argv[2].split('_')[1].split('.')[0]
print 'xstype=', xstype

lines = map(lambda x: x.strip(), f.readlines())

while '-->' not in lines[0]:
  del lines[0]

reaction = lines[0].strip()
del lines[0]
(beam_mass, target_mass, threshold, final_state_multiplicity) = map(float, lines[0].split())
del lines[0:2]
points = []
for l in lines:
  try:
    (x, xmin, xmax, y, yerrp_stat, yerrm_stat, yerrp_sys, yerrm_sys) = map(float, l.split()[1:9])
    xerrm = x - xmin
    xerrp = xmax - x
    author = l.split()[9].strip()
    year = 1900 + int(re.sub('[A-Za-z]+', '', l.split()[10]))
    journal = ' '.join(l.split()[11:]).strip()
    point = {
      'xval' : x,
      'xerrm' : xerrm,
      'xerrp' : xerrp,
      'yval'  : y,
      'yerrp_stat' : yerrp_stat,
      'yerrm_stat' : yerrm_stat,
      'yerrp_sys'  : yerrp_sys,
      'yerrm_sys'  : yerrm_sys,
      'cite' : (author, journal, year)
    }
    points.append(point)
  except:
    print "cannot parse line '{0}'".format(l)

records = {}
for p in points:
  if p['cite'] not in records.keys():
    records[p['cite']] = []
  records[p['cite']].append(p)

for r in records.keys():
  rec = records[r]
  # cite
  try:
    cite_id = cur.execute("select cite_id from cite where author = '{author}' and journal = '{journal}' and year = {year}".format(
      author = rec[0]['cite'][0],
      journal = rec[0]['cite'][1],
      year = rec[0]['cite'][2]
   )).fetchone()[0]
  except:
    cur.execute("insert into cite (author, journal, year) values ('{author}', '{journal}', {year})".format(
      author = rec[0]['cite'][0],
      journal = rec[0]['cite'][1],
      year = rec[0]['cite'][2]
    ))
    cite_id = cur.execute("select max(cite_id) from cite").fetchone()[0]
  # record
  cur.execute("insert into records (type, creation_time, update_time) values ('plot', {0}, {0})".format(time.time()))
  rec_id = cur.execute("select max(rec_id) from records").fetchone()[0]
  # properties
  cur.execute("insert into properties values ({0}, '{1}', '{2}')".format(rec_id, 'reaction', reaction))
  cur.execute("insert into properties values ({0}, '{1}', '{2}')".format(rec_id, 'XS type', xstype))
  cur.execute("insert into properties values ({0}, '{1}', '{2}')".format(rec_id, 'beam_mass', beam_mass))
  cur.execute("insert into properties values ({0}, '{1}', '{2}')".format(rec_id, 'target_mass', target_mass))
  cur.execute("insert into properties values ({0}, '{1}', '{2}')".format(rec_id, 'threshold', threshold))
  cur.execute("insert into properties values ({0}, '{1}', '{2}')".format(rec_id, 'final_state_multiplicity', final_state_multiplicity))
  # cite_records
  cur.execute('insert into cite_records values ({0}, {1})'.format(rec_id, cite_id))
  # plots
  cur.execute("insert into plots values ({0}, '{1}', '{2}', '')".format(rec_id, 'GEV/C', 'MB'))
  # data
  for p in rec:
    cur.execute("insert into data (rec_id, xval, yval, xerrm, xerrp, yerrm_stat, yerrp_stat, yerrm_sys, yerrp_sys) values ({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8})".format(
      rec_id, p['xval'], p['yval'], p['xerrm'], p['xerrp'], p['yerrm_stat'], p['yerrp_stat'], p['yerrm_sys'], p['yerrp_sys']
    ))

db.commit()
f.close()
