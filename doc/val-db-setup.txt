VAL-DB
   =======
	
	Project website:
	----------------
	
        http://val-db.cern.ch
	
	
	Setting up virtual machine:
	---------------------------


Install httpd, php, git:

    $ yum install httpd php git php-pdo

Modify /etc/httpd/conf/httpd.conf adding index.php to DirectoryIndex

DirectoryIndex index.php index.html index.html.var

    $ vi /etc/httpd/conf/httpd.conf


Start httpd:    

    $ /sbin/chkconfig httpd on
    $ /sbin/service httpd start

Disable SElinux modifying /etc/sysconfig/selinux: 

    $ vi /etc/sysconfig/selinux

change SELINUX=disabled and reboot virtual machine:


Disable SLC6 firewall - Type the following two commands (you must login as the root user):
    $ /etc/init.d/iptables save
    $ /etc/init.d/iptables stop

Turn off firewall on boot:
    $ chkconfig iptables off

Check that content of /var/www belongs to apache user

Reboot virtual machine:
    $ reboot